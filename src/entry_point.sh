#!/bin/bash
mknod /dev/loop0 b 7 0
buildiso -i

if [ "$1" == "auto" ]; then
    exec /usr/sbin/crond -n
else
    exec "$@"
fi
